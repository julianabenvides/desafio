/// <reference types="cypress" />

import loc from '../support/locators'
import exemplo from '../fixtures/example.json'

describe ('Challenge', () => {
    // Numero 1
    it ('Click on buttons', () =>{
        cy.visit('https://wj-qa-automation-test.github.io/qa-test/')
        cy.get(loc.BUTTONS.ONE).click().should('not.be.visible')
        cy.get(loc.BUTTONS.TWO).click().should('not.be.visible')
        cy.get(loc.BUTTONS.FOUR).click().should('not.be.visible')
        cy.screenshot('Challenge')
    })
    //Numero 2
    it ('Iframe', () => {
        cy.visit('https://wj-qa-automation-test.github.io/qa-test/')
        cy.get('#iframe_panel_body > iframe').then (iframe => {
          const body = iframe.contents().find('body')
          cy.wrap(body).find('#btn_one').click().should('not.be.visible')
          cy.wrap(body).find('#btn_two').click().should('not.be.visible')
          cy.wrap(body).find('#btn_link').click().should('not.be.visible')
          cy.screenshot('Iframe')
        })
    })
    //Numero 3
    it ('End Scenario', ()=> {
        cy.visit('https://wj-qa-automation-test.github.io/qa-test/')
        cy.get(loc.END.NAME).type(exemplo.name)
        cy.get(loc.BUTTONS.ONE).click()
        cy.get(loc.END.OPTIONTHREE).click()
        cy.get(loc.END.EXAMPLE).select('ExampleTwo')
        cy.get(loc.END.IMGSELENIUM).should('be.visible')
        cy.screenshot('End')
    }) 

})

