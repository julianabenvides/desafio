const locators = {

    BUTTONS:{
        ONE: '#btn_one',
        TWO: '#btn_two',
        FOUR: '#btn_link'
    },

    END:{

        NAME: '#first_name',
        OPTIONTHREE: '#opt_three',
        EXAMPLE: '#select_box',
        IMGSELENIUM: '[alt="selenium"]'

    }

}

export default locators;